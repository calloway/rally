(ns rally.problem2-test
  (:require [clojure.test :refer :all]
            [rally.core :refer :all]
            [rally.problem2 :refer :all]))

(def flush-hand '([9 :clubs] [7 :clubs] [5 :clubs] [3 :clubs] [2 :clubs]))
(def fullhouse-hand '([9 :clubs] [9 :spades] [5 :clubs] [5 :diamonds] [5 :hearts]))
(def straight-hand '([9 :clubs] [7 :clubs] [8 :clubs] [5 :diamonds] [6 :clubs]))
(def straightflush-hand '([9 :clubs] [7 :clubs] [8 :clubs] [5 :clubs] [6 :clubs]))
(def straightflush2-hand '([:A :clubs] [:K :clubs] [:J :clubs] [:Q :clubs] [10 :clubs]))
(def fourofkind-hand '([9 :clubs] [9 :diamonds] [9 :hearts] [9 :spades] [6 :clubs]))
(def threeofkind-hand '([9 :clubs] [9 :diamonds] [9 :hearts] [5 :spades] [6 :clubs]))
(def twoofkind-hand '([9 :clubs] [9 :diamonds] [:J :hearts] [5 :spades] [6 :clubs]))
(def twopair-hand '([9 :clubs] [9 :diamonds] [:J :hearts] [:J :spades] [6 :clubs]))

(deftest hand-test
  (testing "various hands"
    (is (true? (flush? flush-hand)))
    (is (false? (flush? fullhouse-hand)))
    (is (true? (fullhouse? fullhouse-hand)))
    (is (true? (straight? straight-hand)))
    (is (false? (straight? straightflush-hand)))
    (is (false? (straightflush? straight-hand)))
    (is (true? (straightflush? straightflush-hand)))
    (is (false? (flush? straight-hand)))
    (is (false? (straight? straightflush2-hand)))
    (is (true? (straightflush? straightflush2-hand)))
    (is (false? (fourofkind? fullhouse-hand)))
    (is (true? (fourofkind? fourofkind-hand)))
    (is (false? (fourofkind? threeofkind-hand)))
    (is (true? (threeofkind? threeofkind-hand)))
    (is (false? (twoofkind? fullhouse-hand)))
    (is (true? (twoofkind? twoofkind-hand)))
    (is (false? (twoofkind? twopair-hand)))
    (is (true? (twopair? twopair-hand)))
    (is (false? (flush? straight-hand)))))
