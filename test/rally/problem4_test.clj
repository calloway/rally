(ns rally.problem4-test
  (:require [clojure.test :refer :all]
            [rally.core :refer :all]
            [rally.problem4 :refer :all]))

(def testboard [[0 1 0 0 0] [1 0 0 1 1] [1 1 0 0 1] [0 1 0 0 0] [1 0 0 0 1]])

(def simpleboard1 [[1 1 1]
                   [1 1 1]
                   [1 1 1]])

(def simpleboard2 [[0 1 0]
                   [1 1 1]
                   [0 1 0]])

(deftest evolve-tests
  (testing "evolving of board"
    (is (= [[0 0 0 0 0]
            [1 0 1 1 1]
            [1 1 1 1 1]
            [0 1 0 0 0]
            [0 0 0 0 0]] (evolve testboard)))
    (is (= [[1 0 1]
            [0 0 0]
            [1 0 1]] (evolve simpleboard1)))
    (is (= [[1 1 1]
            [1 0 1]
            [1 1 1]] (evolve simpleboard2)))
    ))
