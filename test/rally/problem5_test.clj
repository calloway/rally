(ns rally.problem5-test
  (:require [clojure.test :refer :all]
            [rally.core :refer :all]
            [rally.problem5 :refer :all]))

(deftest parse-variables-test
  (testing "parse-variables"
    (is (= {"day" "Thursday" "name" "Billy"} (parse-variables "(day => Thursday, name => Billy)")))
    ))

(deftest substitutetemplate-test
  (testing "substitutetemplate"
    (is (= "The day is Thursday and my name is Billy" (substitute-all-templates "The day is ${day} and my name is ${name}"
                                   (parse-variables "(day => Thursday, name => Billy)"))))))
