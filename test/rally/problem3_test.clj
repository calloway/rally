(ns rally.problem3-test
  (:require [clojure.test :refer :all]
            [rally.core :refer :all]
            [rally.problem3 :refer :all]))

(deftest spiral-test
  (testing "spirals"
    (is (= "01" (spiral-to-string 1)))
    (is (= "01\n-2" (spiral-to-string 2)))
    (is (= "01\n32" (spiral-to-string 3)))
    (is (= " 6 7 8 9\n 5 0 110\n 4 3 2--" (spiral-to-string 10)))
    ))

(deftest get-dimensions-test
  (testing "get-dimensions"
    (is (= [1 1] (get-dimensions 0)))
    (is (= [2 1] (get-dimensions 1)))
    (is (= [2 2] (get-dimensions 2)))
    (is (= [2 2] (get-dimensions 3)))
    (is (= [3 2] (get-dimensions 4)))
    (is (= [3 2] (get-dimensions 5)))
    (is (= [3 3] (get-dimensions 6)))
    (is (= [3 3] (get-dimensions 7)))
    (is (= [3 3] (get-dimensions 8)))
    (is (= [4 3] (get-dimensions 9)))

    (is (= [4 3] (get-dimensions 10)))
    (is (= [4 3] (get-dimensions 11)))
    (is (= [4 4] (get-dimensions 12)))
    (is (= [4 4] (get-dimensions 13)))
    (is (= [4 4] (get-dimensions 14)))
    (is (= [4 4] (get-dimensions 15)))
    (is (= [5 4] (get-dimensions 16)))
    (is (= [5 4] (get-dimensions 17)))
    (is (= [5 4] (get-dimensions 18)))
    (is (= [5 4] (get-dimensions 19)))
    (is (= [5 5] (get-dimensions 20)))
    (is (= [5 5] (get-dimensions 21)))
    (is (= [5 5] (get-dimensions 22)))
    (is (= [5 5] (get-dimensions 23)))
    (is (= [5 5] (get-dimensions 24)))
    (is (= [6 5] (get-dimensions 25)))
    (is (= [6 5] (get-dimensions 26)))
    (is (= [6 5] (get-dimensions 27)))
    (is (= [6 5] (get-dimensions 28)))
    (is (= [6 5] (get-dimensions 29)))

    ))

(deftest get-y-test
  (testing "get-y, 2x2 tests."
    (is (= 0 (get-y 1 '(2 2))))
    (is (= 1 (get-y 2 '(2 2))))
    (is (= 1 (get-y 3 '(2 2)))))

  (testing "get-y, 3x2 ests."
    (is (= 0 (get-y 1 '(3 2))))
    (is (= 1 (get-y 2 '(3 2))))
    (is (= 1 (get-y 3 '(3 2))))
    (is (= 1 (get-y 4 '(3 2))))
    (is (= 0 (get-y 5 '(3 2)))))

  (testing "get-y, 3x3 ests."
    (is (= 1 (get-y 1 '(3 3))))
    (is (= 2 (get-y 2 '(3 3))))
    (is (= 2 (get-y 3 '(3 3))))
    (is (= 2 (get-y 4 '(3 3))))
    (is (= 1 (get-y 5 '(3 3))))
    (is (= 0 (get-y 6 '(3 3))))
    (is (= 0 (get-y 7 '(3 3))))
    (is (= 0 (get-y 8 '(3 3)))))

  (testing "get-y, 4x3 ests."
    (is (= 1 (get-y 1 '(4 3))))
    (is (= 2 (get-y 2 '(4 3))))
    (is (= 2 (get-y 3 '(4 3))))
    (is (= 2 (get-y 4 '(4 3))))
    (is (= 1 (get-y 5 '(4 3))))
    (is (= 0 (get-y 6 '(4 3))))
    (is (= 0 (get-y 7 '(4 3))))
    (is (= 0 (get-y 8 '(4 3))))
    (is (= 0 (get-y 9 '(4 3))))
    (is (= 1 (get-y 10 '(4 3))))
    (is (= 2 (get-y 11 '(4 3))))

    )
  (testing "get-y, 4x4 ests."
    (is (= 1 (get-y 1 '(4 4))))
    (is (= 2 (get-y 2 '(4 4))))
    (is (= 2 (get-y 3 '(4 4))))
    (is (= 2 (get-y 4 '(4 4))))
    (is (= 1 (get-y 5 '(4 4))))
    (is (= 0 (get-y 6 '(4 4))))
    (is (= 0 (get-y 7 '(4 4))))
    (is (= 0 (get-y 8 '(4 4))))
    (is (= 0 (get-y 9 '(4 4))))
    (is (= 1 (get-y 10 '(4 4))))
    (is (= 2 (get-y 11 '(4 4))))
    (is (= 3 (get-y 12 '(4 4))))
    (is (= 3 (get-y 13 '(4 4))))
    (is (= 3 (get-y 14 '(4 4))))
    (is (= 3 (get-y 15 '(4 4)))))

  

  (testing "get-y, tests."
    (is (= 2 (get-y 5 '(5 5))))
    (is (= 0 (get-y 24 '(5 5))))
    (is (= 1 (get-y 9 '(5 5))))))

(deftest get-x-test
  (testing "get-x, 5x5 tests."
    (is (= 2 (get-x 3 '(5 5))))
    
    (is (= 0 (get-x 20 '(5 5))))
    (is (= 0 (get-x 19 '(5 5))))
    (is (= 1 (get-x 5 '(5 5))))
    (is (= 4 (get-x 11 '(5 5))))    
    (is (= 2 (get-x 14 '(5 5)))))

  (testing "get-x, 2x2 tests."
    (is (= 1 (get-x 1 '(2 2))))
    (is (= 1 (get-x 2 '(2 2))))
    (is (= 0 (get-x 3 '(2 2)))))

  (testing "get-x, 3x2 ests."
    (is (= 2 (get-x 1 '(3 2))))
    (is (= 2 (get-x 2 '(3 2))))
    (is (= 1 (get-x 3 '(3 2))))
    (is (= 0 (get-x 4 '(3 2))))
    (is (= 0 (get-x 5 '(3 2)))))

  (testing "get-x, 3x3 ests."
    (is (= 2 (get-x 1 '(3 3))))
    (is (= 2 (get-x 2 '(3 3))))
    (is (= 1 (get-x 3 '(3 3))))
    (is (= 0 (get-x 4 '(3 3))))
    (is (= 0 (get-x 5 '(3 3))))
    (is (= 0 (get-x 6 '(3 3))))
    (is (= 1 (get-x 7 '(3 3))))
    (is (= 2 (get-x 8 '(3 3))))      
      ))

(deftest get-0-test
  (testing "get-0-pos tests"
    (is (= [0 0] (get-0-pos '(1 1))))
   (is (= [0 0] (get-0-pos '(2 1))))
   (is (= [0 0] (get-0-pos '(2 2))))
   (is (= [1 0] (get-0-pos '(3 2))))
   (is (= [1 1] (get-0-pos '(3 3))))
   (is (= [1 1] (get-0-pos '(4 3))))
   (is (= [1 1] (get-0-pos '(4 4))))
   (is (= [2 1] (get-0-pos '(5 4))))
 
   (is (= [2 2] (get-0-pos '(5 5))))
   (is (= [2 2] (get-0-pos '(6 5))))
   (is (= [2 2] (get-0-pos '(6 6))))
 
    ))

(deftest get-pos-test
  (testing "get-pos various"
    (is (= [0 1] (get-pos 4 '(3 2))))    
    (is (= [1 0] (get-pos 0 '(3 2))))    
    (is (= [0 0] (get-pos 6 '(4 3))))
    (is (= [5 3] (get-pos 1 '(9 8))))    
    )
  (testing "get-pos, 5x5 tests."
    (is (= [3 2] (get-pos 1 '(5 5))))
    (is (= [3 3] (get-pos 2 '(5 5))))
    (is (= [2 3] (get-pos 3 '(5 5))))
    (is (= [1 3] (get-pos 4 '(5 5))))    
    (is (= [1 2] (get-pos 5 '(5 5))))

    (is (= [1 1] (get-pos 6 '(5 5))))
    (is (= [2 1] (get-pos 7 '(5 5))))
    (is (= [3 1] (get-pos 8 '(5 5))))
    (is (= [4 1] (get-pos 9 '(5 5))))
    (is (= [4 2] (get-pos 10 '(5 5))))
    (is (= [4 3] (get-pos 11 '(5 5))))
    (is (= [4 4] (get-pos 12 '(5 5))))
    (is (= [3 4] (get-pos 13 '(5 5))))
    (is (= [2 4] (get-pos 14 '(5 5))))
    (is (= [1 4] (get-pos 15 '(5 5))))
    (is (= [0 4] (get-pos 16 '(5 5))))
    (is (= [0 3] (get-pos 17 '(5 5))))
    (is (= [0 2] (get-pos 18 '(5 5))))
    (is (= [0 1] (get-pos 19 '(5 5))))
    (is (= [0 0] (get-pos 20 '(5 5))))
    (is (= [1 0] (get-pos 21 '(5 5))))
    (is (= [2 0] (get-pos 22 '(5 5))))
    (is (= [3 0] (get-pos 23 '(5 5))))
    (is (= [4 0] (get-pos 24 '(5 5)))))

  (testing "get-pos, 6x5 tests."

    (is (= [1 1] (get-pos 6 '(6 5))))

    (is (= [5 0] (get-pos 25 '(6 5))))
    (is (= [5 1] (get-pos 26 '(6 5))))
    (is (= [5 2] (get-pos 27 '(6 5))))
    (is (= [5 3] (get-pos 28 '(6 5))))
    (is (= [5 4] (get-pos 29 '(6 5))))
    (is (= [5 5] (get-pos 30 '(6 6))))
    (is (= [4 5] (get-pos 31 '(6 6))))
    (is (= [3 5] (get-pos 32 '(6 6))))
    (is (= [2 5] (get-pos 33 '(6 6))))
    (is (= [1 5] (get-pos 34 '(6 6))))
    (is (= [0 5] (get-pos 35 '(6 6))))
    (is (= [0 5] (get-pos 36 '(7 6))))
    (is (= [0 4] (get-pos 37 '(7 6))))
    (is (= [0 3] (get-pos 38 '(7 6))))
    (is (= [0 2] (get-pos 39 '(7 6))))
    (is (= [0 1] (get-pos 40 '(7 6))))
    (is (= [0 0] (get-pos 41 '(7 6))))
    (is (= [0 0] (get-pos 42 '(7 7))))
    (is (= [1 0] (get-pos 43 '(7 7))))
    (is (= [2 0] (get-pos 44 '(7 7))))
    (is (= [3 0] (get-pos 45 '(7 7))))
    (is (= [4 0] (get-pos 46 '(7 7))))
    (is (= [5 0] (get-pos 47 '(7 7))))



    )

  )
