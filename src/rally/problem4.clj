;;; ------------
;;; -Exercise 4-
;;; ------------
;;; Write some code that evolves generations through the "game of life".
;;; The input will be a game board of cells, either alive (1) or dead (0).
;;; The code should take this board and create a new board for the
;;; next generation based on the following rules:1) Any live cell with fewer than two live neighbours dies (under- population)
;;; 2) Any live cell with two or three live neighbours lives on to
;;; the next generation (survival)
;;; 3) Any live cell with more than three live neighbours dies (overcrowding)
;;; 4) Any dead cell with exactly three live neighbours becomes a
;;; live cell (reproduction)
;;; As an example, this game board as input:
;;; 0 1 0 0 0
;;; 1 0 0 1 1
;;; 1 1 0 0 1
;;; 0 1 0 0 0
;;; 1 0 0 0 1
;;; 
;;; will have subsequent generation of:
;;; 0 0 0 0 0
;;; 1 0 1 1 1
;;; 1 1 1 1 1 
;;; 0 1 0 0 0
;;; 0 0 0 0 0

(ns rally.problem4
  (:require [clojure.core.match :refer [match]]
            [clojure.string :refer [join]]))

(def board [[0 1 0 0 0] [1 0 0 1 1] [1 1 0 0 1] [0 1 0 0 0] [1 0 0 0 1]])

(defprotocol revive-rule
  (to-revive? [this board x y])
  (revive-dead [this board]))
  
(defprotocol kill-rule
  (to-die? [this board x y])
  (kill-living [this board]))

(defn living? [board x y]
  (== 1 (get-in board [y x])))
  
(defn width [board] (count (first board)))
(defn height [board] (count (board)))

(defn to-neighbor-val [board x y]
  {:living (living? board x y) :pos [x y]})

(defn ne [board x y]
  (if-not (or (= x (dec (width board))) (zero? y))
    (to-neighbor-val board (inc x) (dec y))))  

(defn nw [board x y]
  (if-not (or (= x 0) (zero? y))
    (to-neighbor-val board (dec x) (dec y))))

(defn n [board x y]
  (if-not (zero? y)
    (to-neighbor-val board x (dec y))))
  
(defn s [board x y]
  (if-not (= (dec (count board)) y)
    (to-neighbor-val board x (inc y))))
 
(defn se [board x y]
  (if-not (or (= x (dec (count (first board)))) (= (dec (count board)) y))
    (to-neighbor-val board (inc x) (inc y)))) 
    
(defn sw [board x y]
  (if-not (or (zero? x) (= (dec (count board)) y))
    (to-neighbor-val board (dec x) (inc y))))
  
(defn e [board x y]
  (if-not (= (dec (count (first board))) x)
     (to-neighbor-val board (inc x) y)))

(defn w [board x y]
  (if-not (zero? x)
    (to-neighbor-val board (dec x) y)))

(defn neighbors [board x y]
  (remove nil? (list  (nw board x y) (ne board x y) (n board x y) (s board x y) (se board x y) (sw board x y) (w board x y) (e board x y))))

(defn- helper-now [this board val to-xxx?]
  (keep-indexed (fn [y ls] (keep-indexed (fn [x c]
                                          (if (to-xxx? this board x y)
                                            val
                                            c)) ls)) board))

(defn kill-now [this board]
  (helper-now this board 0 to-die?))
                                 
(defn revive-now [this board]
  (helper-now this board :revive to-revive?))
  
(deftype livingdies-rule []
  kill-rule
  (to-die? [this board x y]
    (living?  board x y ))
  (kill-living [this board]
    (kill-now this board)))

(deftype underpopulation-rule []
  kill-rule
  (to-die? [this board x y]
    (let [n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (< (count living-neighbors) 2)))
  (kill-living [this board]
    (kill-now this board)))

(deftype overpopulation-rule []
  kill-rule
  (to-die? [this board x y]
    (let [n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (> (count living-neighbors) 3)))
  (kill-living [this board]
    (kill-now this board)))

(deftype reproduction-rule []
  revive-rule
  (to-revive? [this board x y]
    (let [dead? (not (living? board x y))
          n (neighbors board x y)
          living-neighbors (filter (fn[{l :living p :pos}] (true? l)) n)]
      (and dead? (== (count living-neighbors) 3))))
  (revive-dead [this board]
    (revive-now this board)))
  
(def deathrules (list (overpopulation-rule.) (underpopulation-rule.)))

(def reviverules (list (reproduction-rule.)))

(defn combine-results [result op]
  (reduce (fn[m1 m2] (map (fn [l1 l2] (map op l1 l2)) m1 m2)) result))

(defn print-board [board]
  (println (join "\n"  (map #(apply str %) board))))

(defn evolve [board]  
  (combine-results (cons  (combine-results (map (fn [rule] (kill-living rule board)) deathrules) bit-and)
                          (list (combine-results (map (fn [rule] (revive-dead rule board)) reviverules) bit-or))) 
                   (fn [a b] (if (= :revive b) 1 a))))

(defn -main [& args]
  (println "initial board")
  (print-board board)
  (println "evolved board")
  (print-board (evolve board))
  )
