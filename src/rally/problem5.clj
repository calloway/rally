;;;  ------------
;;;  -Exercise 5-
;;;  ------------
;;;  Write some code that can be used in a templating engine.
;;;  This should take a map of variables ("day" => "Thursday", "name"
;;;  => "Billy") as well as a string
;;;  template ("${name} has an appointment on ${Thursday}") and
;;;  perform substitution as needed.
;;;  This needs to be somewhat robust, throwing some kind of error if
;;;  a template uses a variable that has not
;;;  been assigned, as well as provide a way to escape the strings
;;;  ("hello ${${name}}" -> "hello ${Billy}")

(ns rally.problem5
  (:require [clojure.core.match :refer [match]]
            [clojure.string :refer [split join]]))

(defn template? [input]
  (not (nil? (re-matches #".*\$\{([a-zA-Z]*)}.*" input))))

(defn substitute-one-template [input vars]
  (let [thevariable (second  (re-matches #".*\$\{([a-zA-Z]*)}.*" input))
        to-replace (str "${" thevariable "}")]
    (if (contains? vars thevariable)
      (clojure.string/replace input to-replace (get vars thevariable))
      (do
        (println "ERROR: could not find a subsitution for variable " thevariable)
        input))))

(defn parse-variables [input]
  (apply merge (for [x (split (second (re-matches #".*\((.+)\).*" input))
                              #", *")]
                 (-> {} (assoc (first (split x #" *=> *")) (second (split x #" *=> *")))))))

(defn substitute-all-templates [input variables]
  (join " " (map #(if (template? %)
          (substitute-one-template % variables)
          %) (split input #" +"))))

(defn -main [& args]
  (let [variables (parse-variables (if (first args) (first args) "(day => Thursday, name => Billy)"))
        input (if (second args) (second args) "${name} has an appointment on ${Thursday}")]
    (println  (substitute-all-templates input variables))))
