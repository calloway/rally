;;; ------------
;;; -Exercise 3-
;;; ------------
;;; Write some code that accepts an integer and prints the integers
;;; from 0 to that input integer in a spiral format.
;;; For example, if I supplied 24 the output would be:
;;; 20 21 22 23 24
;;; 19 6 7 8 9
;;; 18 5 0 1 10
;;; 17 4 3 2 11
;;; 16 15 14 13 12

(ns rally.problem3
  (:require [clojure.string :refer [split join]]))

(defn get-dimensions [n]
  (cond (zero? n)
        [1 1]
        (= 1 n)
        [2 1]
        :else
        (let [width (inc (last (filter #(<= (* % %) n) (range n))))
              height (inc (quot n width))]
          [width height])))
  
(defn get-adjusted [outer inner]
  (let [x (- outer inner)
        dict {0 0 1 1 2 1 3 2 4 2 5 3 6 3 7 4 8 4 9 5 10 5 11 6 12 6 13 7 14 7}]
    (if (zero? x) 0
        (get dict x))))

(defn get-0-pos [[width height]]
  (let [f #(if (integer? (/ % 2)) (dec (/ % 2)) (quot % 2))]
    [(f width) (f height)]))

(defn get-y [n [width height]]
  (let [[nwidth nheight] (get-dimensions n)]
    (cond
     (and (odd? nwidth) (even? nheight))
     (+ (get-adjusted height nheight) (dec (- nheight (rem n nheight))))
     (and (even? nwidth) (even? nheight))
     (+ (get-adjusted height nheight) (dec nheight))
     (and (= height nheight) (odd? nwidth) (odd? nheight))
     (- height nheight)
     (and (> height nheight) (odd? nwidth) (odd? nheight))
     (quot (- height nheight) 2)
     (and (even? nwidth) (odd? nheight))
     (+ (quot (- height nheight) 2) (rem n nheight)))))

(defn get-x [n [width height]]
  (let [[nwidth nheight] (get-dimensions n)]
    (cond (and (even? nwidth) (odd? nheight))
          (+ (get-adjusted width nwidth) (dec nwidth))
          (and (even? nwidth) (even? nheight))
          (+ (get-adjusted width nwidth) (dec (- nwidth (rem n nheight))))                (and (odd? nwidth) (odd? nheight))         
          (+ (quot (- width nwidth) 2) (rem n nheight))          
          (and (odd? nwidth) (even? nheight))
          (quot (- width nwidth) 2))))

(defn get-pos [n dim]
  [(get-x n dim) (get-y n dim)])

(defn- pad [x padding]
  (format (str "%" padding "d") x))

(defn get-spiral [n & {:keys [padding] :or {padding 3}}]
  (into {} (cons [(get-0-pos (get-dimensions n)) (pad 0 padding)] (for [i (range 1 (inc n))] [(get-pos i (get-dimensions n)) (pad i padding)]))))

(defn spiral-to-string [n]
  (let [padding (count (str n))
        filler (apply str (repeat padding "-"))
        spiral (get-spiral n :padding padding)
        dim (get-dimensions n)   
        tmp (group-by #(identity (second %))
                          (apply concat (for [x (range (first dim))
                                              y (range (second dim))]
                                          (-> '() (conj [x y])))))]
    (clojure.string/join "\n" (map #(apply str %) (map (fn [row] (map (fn[dim] (if (contains? spiral dim) (get spiral dim) filler)) row)) (map #(second %) tmp))))))

(defn print-spiral [n]
  (do
    (println "SPIRAL TO " n)
    (println "------------")
    (println (spiral-to-string n))
    (println "")))

(defn -main [& args]
  (print-spiral 2)
  (print-spiral 9)
  (print-spiral 14)
  (print-spiral 24)
  (print-spiral 64)
  (print-spiral 120)
  )
