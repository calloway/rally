;;; ------------
;;; -Exercise 1-
;;; ------------
;;; Write some code that will accept an amount and convert it to the
;;; appropriate string representation.
;;; Example:
;;; Convert 2523.04
;;; to "Two thousand five hundred twenty-three and 04/100
;;; dollars"

;;; user> (rally.problem1/to-string "0.04")
;;; "zero and 04/100"
;;; user> (rally.problem1/to-string "2523.04")
;;; "two thousand five hundred twenty three and 04/100"

(ns rally.problem1
  (:require [clojure.string :refer [split join]]
            [clojure.core.match :refer [match]]))

(defn over-tendollar-to-string [input]
  (get {2 "twenty" 3 "thirty" 4 "fourty" 5 "fivety" 6 "sixty" 7 "seventy" 8 "eightty" 9 "ninety"} input))

(defn dollar-to-string [input]
  (get {0 nil 1 "one" 2 "two" 3 "three" 4 "four" 5 "five" 6 "six" 7 "seven" 8 "eight" 9 "nine"} input))

(defn tendollar-to-string [_ single]
  (cond (= single 1) "eleven"
        (= single 2) "twelve"
        (= single 3) "thirteen"
        :else (str (dollar-to-string single) "teen")))

(defn hundreddollar-to-string [input]
  (if-let [digit (dollar-to-string input)]
    (str digit " hundred")))

(defn thousanddollar-to-string [input]
  (if-let [digit (dollar-to-string input)]
    (str digit " thousand")))

(defn cents-to-string [cents]
  (str (format "%02d" cents) "/100"))

(defn digits [n]
  (if (< n 10)
    [n]
    (conj (digits (quot n 10)) (rem n 10))))

(defn lessthenhundred-to-string [ls]
  (match ls
         [0 single] (dollar-to-string single)
         [1 1] "eleven"
         [1 2] "twelve"
         [1 3] "thirteen"
         [1 0] "ten"
         [x single] (str (over-tendollar-to-string x) (dollar-to-string single))
         [single] (dollar-to-string single)
         [] nil
         :else nil))

(defn remove-leading-zeros [x]
  (match (vec x)
         [\0 single] (str single)
         :else x))

(defn to-string [input]
  (let [[dollarsstr, centsstr] (split input #"\.")
        dollars (digits (read-string dollarsstr))
        cents (read-string (remove-leading-zeros centsstr))
        tendollars (into [] (take-last 2 dollars))
        overtendollars (into [] (drop-last 2 dollars))
        dollar-fns (take-last (count overtendollars) [thousanddollar-to-string hundreddollar-to-string])
        ls (flatten (cons (map (fn [meth dollar] (meth dollar)) dollar-fns overtendollars)
                          (list (lessthenhundred-to-string tendollars) (cents-to-string cents))))
        cleaned (filter #(not (nil? %)) ls)]
    (join " " (drop-last 1 (interleave cleaned (repeat (count cleaned) "and"))))))

(defn -main [& args]
  (println "2523.04: " (to-string "2523.04"))
  (println "2023.04: " (to-string "2023.04"))
  (println "1.99: " (to-string "1.99"))
  (println "0.99: " (to-string "0.99"))
  (println "20.00: " (to-string "20.00"))
  (println "100.01: " (to-string "100.01"))
  )
