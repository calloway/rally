;; ------------
;; -Exercise 2-
;; ------------
;; Write some code that will evaluate a poker hand and determine its
;; rank.
;; Example:
;; Hand: Ah As 10c 7d 6s (Pair of Aces)
;; Hand: Kh Kc 3s 3h 2d (2 Pair)
;; Hand: Kh Qh 6h 2h 9h (Flush)

(ns rally.problem2
  (:require [clojure.string :refer [split join]]
            [clojure.core.match :refer [match]]))

(defn rank-to-string [r]
  (match r
         :J "Jacks"
         :Q "Queens"
         :K "Kings"
         :A "Aces"
         :else (str r "'s")))

(defn rank [x]
  (match (first x)
         :J 11
         :Q 12
         :K 13
         :A 14
         :else (first x)))

(defn suit [s]
  (match s
         :c :clubs
         :d :diamonds
         :h :hearts
         :s :spades))

(defn inrankedorder? [hand]
  (let [sorted  (sort-by rank hand)
        highest (last sorted)
        lowest (first sorted)
        unique? #(= 5 (count (group-by (fn[[k v]] k) %)))]
    (unique? hand) (== 4 (- (rank highest) (rank lowest)))))

(defn samesuit? [hand]
  (== 1 (count (group-by (fn[[k v]] v) hand))))

(defn flush? [hand]
  (and (not (inrankedorder? hand))
       (samesuit? hand)))

(defn straight? [hand]
  (and (not (samesuit? hand)) (inrankedorder? hand)))

(defn straightflush? [hand]
  (and (samesuit? hand) (inrankedorder? hand)))

(defn get-nofkind [hand n]
  (filter (fn [[k v]] (== (count v) n)) (group-by (fn[[k v]] k) hand)))

(defn nofkind? [hand n]
  (not (empty? (filter (fn [[k v]] (== (count v) n)) (group-by (fn[[k v]] k) hand)))))

(defn fullhouse? [hand]
  (and (nofkind? hand 2) (nofkind? hand 3)))
 
(defn fourofkind? [hand]
  (nofkind? hand 4))

(defn threeofkind? [hand]
  (and (not (fullhouse? hand))
       (nofkind? hand 3)))

(defn twopair? [hand]
  (and (not (fourofkind? hand))
       (== 2 (count ( get-nofkind hand 2)))
    ))

(defn twoofkind? [hand]
  (and (not (fullhouse? hand))
       (not (twopair? hand))
       (nofkind? hand 2)))

(defn to-string [hand]
  (apply str hand))
  
(defn to-rank [hand]
  (let [helper (fn [n & {:keys [which] :or {which first}}] (rank-to-string (which (map key (get-nofkind hand n)))))]
  (cond (twoofkind? hand)
        (str "(Pair of " (helper 2) ")")
        (twopair? hand)
        (str "(Two Pairs " (helper 2) " and " (helper 2 :which second) ")")
        (threeofkind? hand)
        (str  "(3 of kind " (helper 3) ")")
        (fourofkind? hand)
        (str  "(4 of kind " (helper 4) ")")
        (fullhouse? hand)
        "(Full House)"
        (flush? hand)
        "(Flush)"
        (straight? hand)
        "(Straight)"
        (straightflush? hand)
        "(Straight Flush)"
        :else
        "(High card)")))

(defn parse-card [card]
  (let [[r s] (rest (first (re-seq #"([0-9|KJQA]*)([hcds])" card))) 
        rparsed (read-string r)
        rank (if (integer? rparsed) rparsed (keyword rparsed) )
        suit (keyword s)]
    [rank suit]))

(defn parse-hand [input]
  (map parse-card (split input #" ")))

(defn print-rank [input]
  (println  input (to-rank (parse-hand input))))

(defn -main [& args]
  (println "examples given")
  (print-rank "Ah As 10c 7d 6s")
  (print-rank "Kh Kc 3s 3h 2d")
  (print-rank "Kh Qh 6h 2h 9h")
  (println "some more examples")
  (print-rank "Ah Kh Jh Qh 10h")
  (print-rank "Jh Jh 10c 10d Jh")
  (print-rank "3h 3s 3c 10d 6s")
  (print-rank "3h 3s 3c 3d 6s")
  (print-rank "3h 2s 7c 9d 6s")
  )
